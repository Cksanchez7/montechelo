
## Prueba Montechelo

Test Cindy Sanchez

El API esta elaborada en PHP ^7.3 con Laravel Framework 8.51.0

- Es necesario instalar PHP, Apache y MySql, todo esto se encuentra en la herramienta WAMP Server o XAMPP


+ Una vez se clone el proyecto, es necesario

	- crear una base de datos con el nombre de su preferencia, ejemplo: montechelo
	- configurar el archivo .env (existe un .env.example del que se podra guiar), alli debera colocar las credenciales de la base de datos para poder acceder.
	
+ [Para correr el API en un servidor local, es necesario ajustar la configuracion de php.ini, siga el ejemplo] (https://www.bufa.es/goutte-fatal-error-curl-error-60-ssl-certificate-problem-unable-to-get-local-issuer-certificate-in-requestexception-php/)

+ Luego debera abrir una terminal y ejecutar los siguietes comandos

	- composer install (para instalar todas los paquetes configurados en el archivo composer.json )
	- php artisan migrate (para crear la estructura de tablas en nuestra base de datos)
	- php artisan serve (para levantar el servidor)


+ Finalmente el API se consumira a traves de una ruta (http://127.0.0.1:8000/api/voices), enviando dos parametros requeridos que son

	- clienId
	- text

Puede realizar pruebas atraves de Postman.