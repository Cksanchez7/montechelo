<?php

namespace App\Http\Controllers;

use App\Http\Services\TextToVoiceServices;
use App\Http\Services\VoicesServices;
use App\Models\Voices;
use Illuminate\Http\Request;
use  \Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class VoiceController extends Controller
{
    /**
     * @var string[]
     * reglas de validcion
     */
    private $rules = [
        'clientId' => 'required',
        'text' => 'required|max:1000'
    ];

    /**
     * @var VoicesServices
     */
    private $voicesServices;

    /**
     * @var TextToVoiceServices
     */
    private $textToVoiceServices;

    public function __construct(VoicesServices $voicesServices, TextToVoiceServices $textToVoiceServices)
    {
        $this->voicesServices = $voicesServices;
        $this->textToVoiceServices = $textToVoiceServices;
    }

    /**
     * TODO metodo que recibe los parametros a travez de un request, valida que la informacion sea correcta y utiliza los servicios de creacion y conversion de texto a voz
     * @param Request $request
     * @return JsonResponse
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     */
    public function voices(Request $request): JsonResponse
    {
        $validated = Validator::make($request->all(), $this->rules);
        /**
         * revision de las validaciones
         */
        if ($validated->fails()) {
            /**
             * respuesta en formato json del error con estado 400
             */
            return response()->json($validated->messages(), Response::HTTP_BAD_REQUEST);
        }

        ///consumo del servicio para la persistencia del cliente y el texto
        $this->voicesServices->create($request->get('clientId'), $request->get('text'));

        ///consumo del servicio para la conversion de texto a archivo de voz.
        $this->textToVoiceServices->convert($request->get('text'), $request->get('clientId'));

        //Respuesta en formato json con status 201
        return response()->json("El archivo se ha generado exitosamente en la carpeta public->audio.", 201);
    }
}
