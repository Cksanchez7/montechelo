<?php

namespace App\Http\Repositories;

use App\Models\Voices;

class VoicesRepository
{
    //metodo para persistir data a travez del modelo
    public function create(string $clientId, string $text){
        Voices::firstOrCreate(
            [
                'clientId' => $clientId,
                'text' => $text
            ]
        );
    }
}
