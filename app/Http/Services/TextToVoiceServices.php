<?php

namespace App\Http\Services;

use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;


class TextToVoiceServices
{
    /**
     * metodo para convertir de texto a voz, y generar archivo .mp3
     * @throws \Google\ApiCore\ApiException
     * @throws \Google\ApiCore\ValidationException
     */
    public function convert(string $text, string $clientId) {
        //. autenticacion para los servicios de google
        $textToSpeechClient = new TextToSpeechClient([
            'credentials' => json_decode(file_get_contents(__DIR__.'/keys.json'), true)
        ]);

        $input = new SynthesisInput();
        $input->setText($text);
        $voice = new VoiceSelectionParams();
        $voice->setLanguageCode('en-US');
        $audioConfig = new AudioConfig();
        $audioConfig->setAudioEncoding(AudioEncoding::MP3);
        $resp = $textToSpeechClient->synthesizeSpeech($input, $voice, $audioConfig);

        ///validacion de que exista el folder
        if (!is_dir('audio/')) {
            ///creacion del folder
            mkdir('audio/');
        }
        //creacion del archivo de voz
        file_put_contents('audio/audio-'.$clientId.'.mp3', $resp->getAudioContent());
    }

}
