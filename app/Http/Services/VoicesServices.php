<?php

namespace App\Http\Services;

use App\Http\Repositories\VoicesRepository;

class VoicesServices
{
    /**
     * @var VoicesRepository
     */
    private $voicesRepository;

    public function __construct(VoicesRepository $voicesRepository)
    {
        $this->voicesRepository = $voicesRepository;
    }

    /**
     * @param string $clientId
     * @param string $text
     */
    public function create(string $clientId, string $text)
    {
        /// envio de parametros al repositorio de voices para la persistencia de datos
        $this->voicesRepository->create($clientId, $text);
    }

}
