<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voices extends Model
{
    protected $fillable = ['clientId','text'];
    use HasFactory;
}
